#include "main.h"
#include <Arduino.h>
#include "IRSensor.h"
#include "Servo.h"
#include "ServoUtils.h"
#include <fix16.h>
#include "ScanUtils.h"
#include <avr/pgmspace.h>
#include "PointCloud.h"
#include "PrintUtil.h"
#include "SparkFun_VL53L1X.h"
#include <Wire.h>

VL53L1XWrapper sensor;
Servo servo_pan;
Servo servo_tilt;

// List of the angles we're going to be scanning. Storing values in microseconds
//const uint16_t panAngles[NUM_PAN_POINTS];
//const uint16_t tiltAngles[NUM_TILT_POINTS];

/*
 * Due to the fact that AVR uses a nonunified address space, we only
 * get about 8K of actual R/W memory. As such, we need to be VERY 
 * careful with how we use space. Normally, we'd have a list of pan angles
 * to scan, a list of tilt angles to scan, a 2d array of the raw returned
 * data, and then a list of the 3d points in the resulting point cloud.
 *
 * While we could just malloc/free, that's kind of a pain because GCC gets
 * angry and can't really give us an accurate estimate of memory usage.
 *
 * Instead, we're going to use a single array (scanData) for everything.
 *
 * When we're getting polar scan data, x=Θ, y=φ, z=r
 */
Point __used scanData[NUM_PAN_POINTS][NUM_TILT_POINTS];

static inline uint16_t getSensorDistance() {return sensor();};
//static inline uint16_t getSensorDistance() {return 2000;};

void setup() 
{
    // Init serial at 115.2 kbaud
    Serial.begin(115200);
    Wire.begin();
    pinMode(LED_PIN, OUTPUT);
    pinMode(BUZZER_PIN, OUTPUT);
    pinMode(SERVO_POWER_FET_PIN, OUTPUT);
    setLED(true);
    pulseTone(500, 100);
    pulseTone(1000, 100);
    setLED(false);

    //sensor.checkLUT();
    //while (1) Serial.println(getDistance(*getSensorDistance));

    // Init servos
    servo_pan.attach(PAN_SERVO_PORT);
    servo_tilt.attach(TILT_SERVO_PORT);

    // Fill scanData with pan/tilt angles
    fillScanAnglesList(
            scanData, 
            PAN_ANGLE_MIN, PAN_ANGLE_MAX, NUM_PAN_POINTS,
            TILT_ANGLE_MIN, TILT_ANGLE_MAX, NUM_TILT_POINTS
            );

    //Point *pt = &(scanData[0][0]);
    //servo_pan.writeMicroseconds(panAngleToServoMicroseconds(pt->theta));
    //servo_tilt.writeMicroseconds(tiltAngleToServoMicroseconds(pt->phi));
    //servo_pan.writeMicroseconds(panAngleToServoMicroseconds(fix16_to_fix8(PAN_ANGLE_MAX)));
    //servo_tilt.writeMicroseconds(tiltAngleToServoMicroseconds(fix16_to_fix8(float2fix16(0))));
    
    // Scan all points
    scanAll(scanData, &getSensorDistance, &servo_pan, &servo_tilt);

    // Convert point cloud to cartesian
    pointCloudToCartesian(scanData);

    // Print everything out to serial
    printPointCloud(scanData);
}

void loop(){}

/*
void loop() 
{
    for (uint16_t i=0; i<NUM_PAN_POINTS; i++) {
        for (uint16_t j=0; j<NUM_TILT_POINTS; j++) {
            char buf[50];
            Point pt = scanData[i][j];
            fix16_t theta = fix16_from_float(fix8_to_float(pt.theta));
            fix16_t phi = fix16_from_float(fix8_to_float(pt.phi));
            uint16_t r = pt.r;
            char theta_buf[20];
            char phi_buf[20];
            fix16_to_str(theta, theta_buf, 4);
            fix16_to_str(phi, phi_buf, 4);
            sprintf(buf, "PAN: %d, TILT: %d", i, j);
            Serial.println(buf);
            sprintf(buf, "Theta: %s, Phi: %s", theta_buf, phi_buf);
            Serial.println(buf);
            Serial.println("");
            delay(1000);
        }
    }
}
*/
