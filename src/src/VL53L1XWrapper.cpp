#include "VL53L1XWrapper.h"
#include <Arduino.h>
#include "main.h"
#include "fix16.h"

VL53L1XWrapper::VL53L1XWrapper() {
    uint16_t k[VL_LUT_SIZE];
    uint16_t v[VL_LUT_SIZE];
    for (uint16_t i=0; i<VL_LUT_SIZE; i++) {
        k[i] = VL53L1X_LUT[i].k;
        v[i] = VL53L1X_LUT[i].v;
    }
    m = new Interp1d_u16<VL_LUT_SIZE>(k, v);
    pinMode(VL53L1X_PWM_PIN, INPUT);
}

//uint16_t VL53L1XWrapper::operator()(uint16_t adc) {
//    return (*m)(adc);
//}

void VL53L1XWrapper::checkLUT() {
    for (uint16_t i=0; i<4096; i++) {
        char buf[50];
        sprintf(buf, "In: %d Out: %d", i, (*m)(i));
        Serial.println(buf);
    }
}

uint16_t VL53L1XWrapper::operator()() {
    uint16_t len_high = pulseIn(VL53L1X_PWM_PIN, LOW); // Inverted b/c of level shifter
    uint16_t len_low = pulseIn(VL53L1X_PWM_PIN, HIGH);
    uint16_t dist = fix16_to_int(fix16_mul( // Duty cycle * 4000mm
                fix16_div(fix16_from_int(len_high), 
                    fix16_add(fix16_from_int(len_low), fix16_from_int(len_high))), fix16_from_int(4000)));

    return dist;
}
