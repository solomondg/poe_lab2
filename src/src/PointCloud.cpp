#include "PointCloud.h"
#include "PrintUtil.h"

void pointCloudToCartesian(Point array[NUM_PAN_POINTS][NUM_TILT_POINTS]) {
    Point *pt;
    for (uint8_t x=0; x<NUM_PAN_POINTS; x++) {
        for (uint8_t y=0; y<NUM_TILT_POINTS; y++) {
            pt = &(array[x][y]);
            if (pointValid(pt))
                polarToCartesian(pt);
            else
                *pt = POINT_ORIGIN;
        }
    }
}

void polarToCartesian(Point *pt) { // TODO consider optimization
    fix16_t theta = fix16_div(fix16_mul(fix8_to_fix16(pt->theta), fix16_from_float(M_PI)), fix16_from_int(180));
    fix16_t phi = fix16_div(fix16_mul(fix16_sub(fix16_from_int(90), fix8_to_fix16(pt->phi)), fix16_from_float(M_PI)), fix16_from_int(180));
    // right now, phi is angle from horizontal. We need to convert to angle from vertical.
    // To do so, we just add pi/2 (90 degrees)
    //phi = fix16_add(phi, fix16_div(fix16_from_float(M_PI),2));
    uint16_t r = pt->r;

    // x = r sin(φ) cos(θ)
    // y = r sin(φ) sin(θ)
    // z = r cos(φ)
    
    fix16_t sinphi = fix16_sin(phi);
    fix16_t cosphi = fix16_cos(phi);
    fix16_t sintheta = fix16_sin(theta);
    fix16_t costheta = fix16_cos(theta);

    fix16_t sinphi_costheta = fix16_mul(sinphi, costheta);
    fix16_t sinphi_sintheta = fix16_mul(sinphi, sintheta);

    int16_t x = fix16_to_int(fix16_mul(fix16_from_int(r), sinphi_costheta));
    int16_t y = fix16_to_int(fix16_mul(fix16_from_int(r), sinphi_sintheta));
    int16_t z = fix16_to_int(fix16_mul(fix16_from_int(r), cosphi));

    char buf[50];
    char th[8];
    char ph[8];
    fix82str(th, pt->theta);
    fix82str(ph, pt->phi);
    sprintf(buf, "θ=%s°, φ=%s°, r=%dmm, x=%dmm, y=%dmm, z=%dmm", th, ph, pt->r, x, y, z);
    Serial.println(buf);

    pt->x = x;
    pt->y = y;
    pt->z = z;
}
