#include "IRSensor.h"
#include "main.h"

IRSensor::IRSensor(uint16_t p) {
    port = p;
    uint16_t k[SENSOR_LUT_SIZE];
    uint16_t v[SENSOR_LUT_SIZE];
    for (uint16_t i=0; i<SENSOR_LUT_SIZE; i++) {
        k[i] = SENSOR_LUT[i].k;
        v[i] = SENSOR_LUT[i].v;
    }
    m = new Interp1d_u16<SENSOR_LUT_SIZE>(k, v);
}

uint16_t IRSensor::operator()(uint16_t adc) {
    return (*m)(adc);
}
uint16_t IRSensor::operator()() {
    uint16_t adc = analogRead(port);
    return (*m)(adc);
}
