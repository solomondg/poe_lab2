#include <Arduino.h>
#include "main.h"
#include <fix8.h>
#include "PrintUtil.h"
#include "PointCloud.h"

void printPointCloud(Point array[NUM_PAN_POINTS][NUM_TILT_POINTS]) {
    Serial.print(jsonStartText);
    char buf[50];
    char buf1[fix8bufsize];
    char buf2[fix8bufsize];
    char buf3[fix8bufsize];
    Point *pt;
    for (uint8_t x=0; x<NUM_PAN_POINTS; x++) {
        for (uint8_t y=0; y<NUM_TILT_POINTS; y++) {
            pt = &(array[x][y]);
            if (pt->x == POINT_ORIGIN.x && pt->y == POINT_ORIGIN.y && pt->z == POINT_ORIGIN.z) continue;
            sprintf(buf, 
                    (x == NUM_PAN_POINTS-1 && y==NUM_TILT_POINTS-1) ? listEntryTextEnd : listEntryText,
                    pt->x, pt->y, pt->z
                    );
            Serial.print(buf);
        }
    }
    Serial.println(jsonEndText);
}
