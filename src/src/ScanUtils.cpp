#include "ScanUtils.h"
#include "PrintUtil.h"
#include "PointCloud.h"

void fillScanAnglesList(
        Point array[NUM_PAN_POINTS][NUM_TILT_POINTS],
        fix16_t minPanAngle, fix16_t maxPanAngle, uint8_t numPanPoints,
        fix16_t minTiltAngle, fix16_t maxTiltAngle, uint8_t numTiltPoints
        ) {
    fix16_t panDelta = fix16_div(fix16_sub(maxPanAngle, minPanAngle), fix16_from_int(numPanPoints-1));
    fix16_t tiltDelta = fix16_div(fix16_sub(maxTiltAngle, minTiltAngle), fix16_from_int(numTiltPoints-1));

    /*
    char buf[50];
    char theta[8];
    char phi[8];
    fix16_to_str(panDelta, theta, 4);
    fix16_to_str(tiltDelta, phi, 4);
    sprintf(buf, "pan delta: %s, tilt delta: %s", theta, phi);
    Serial.println(buf);
    */

    Serial.println("Filling Scan Buffer:");
    Serial.print("[");
    for (uint16_t i=0; i<numPanPoints; i++) Serial.print("-");
    Serial.print("]");
    Serial.print('\r');
    Serial.print("[");
    for (uint16_t panIdx=0; panIdx<numPanPoints; panIdx++) {
        for (uint16_t tiltIdx=0; tiltIdx<numTiltPoints; tiltIdx++) {
            Point p;
            p.theta                = fix8_from_float(fix16_to_float(fix16_add(minPanAngle, fix16_mul(fix16_from_int(panIdx), panDelta))));
            p.phi                  = fix8_from_float(fix16_to_float(fix16_add(minTiltAngle, fix16_mul(fix16_from_int(tiltIdx), tiltDelta))));
            p.r                    = 100;
            array[panIdx][tiltIdx] = p;

            char buf[50];
            char theta[8];
            char phi[8];
            fix82str(theta, p.theta);
            fix82str(phi, p.phi);
            sprintf(buf, "pan idx %d/%d, tilt idx %d/%d, theta %s, phi %s", panIdx, numPanPoints, tiltIdx, numTiltPoints, theta, phi);
            //Serial.println(buf);
            //delay(300);
        }
        Serial.print("#");
    }
    Serial.println("\r\nDone!");
}

uint16_t getDistance(uint16_t (*distFcn)()) {
    unsigned long incr = distFcn();    
    for (uint8_t i=0; i<NUM_DISTANCE_READINGS_TO_AVERAGE-1; i++) {
        delay(TIME_BETWEEN_READINGS_MS);
        incr = incr + distFcn();
    }
    incr = (incr)/NUM_DISTANCE_READINGS_TO_AVERAGE;
    return (uint16_t) (incr & 0xFFFF); // Yoink lower 16 bits
}

void scanAll(
        Point array[NUM_PAN_POINTS][NUM_TILT_POINTS],
        uint16_t (*distFcn)(),
        Servo *panServo,
        Servo *tiltServo
        ) {
    Point *pt;
    uint16_t idx = 0;
    for (uint16_t x=0; x<NUM_PAN_POINTS; x++) {
        for (uint16_t y=0; y<NUM_TILT_POINTS; y++) {
            idx++;
            pt = &(array[x][y]);
            uint16_t dist = moveAndScan(
                    pt->theta, pt->phi, 
                    distFcn, panServo, tiltServo,
                    DWELL_MS + (y==0) ? ROLLOVER_ADD_DWELL_MS : 0
                    );
            pt->r = dist;
            setLED(true);
            //setTone((y==NUM_TILT_POINTS-1) ? 1000 : 500);
            delay(20);
            setLED(false);
            setTone(0);
            char buf[50];
            char theta[10];
            char phi[10];
            fix82str(theta, pt->theta);
            fix82str(phi, pt->phi);
            sprintf(buf, "θ=%s°, φ=%s°, r=%dmm, point %d/%d %s", theta, phi, pt->r, idx, ((uint16_t) NUM_PAN_POINTS)*((uint16_t)NUM_TILT_POINTS), pointValid(pt) ? "✓" : "X");
            Serial.println(buf);
        }
        delay(ROLLOVER_ADD_DWELL_MS);
    }
}
