#ifndef utils_h
#define utils_h

#include <Arduino.h>
#include "fix8.h"
#include "fix16.h"
#include "sin_lut1.h"

constexpr fix16_t float2fix16(float x) {
    float temp = x * fix16_one;
#ifndef FIXMATH_NO_ROUNDING
    temp += (temp >= 0) ? 0.5f : -0.5f;
#endif
    return (fix16_t)temp;
}

constexpr fix16_t int2fix16(int x) {
    return x * fix16_one;
}

constexpr fix8_t uint82fix8(uint8_t x) {
    return x*fix8_one;
}

constexpr fix8_t float2fix8(float x) {
    float temp = x * fix8_one;
#ifndef FIXMATH_NO_ROUNDING
    temp += (temp >= 0) ? 0.5f : -0.5f;
#endif
    return (fix8_t)temp;
}

constexpr fix8_t int2fix8(int x) {
    return x * fix8_one;
}

static inline __attribute__ ((hot)) fix8_t fucking_stupid_bullshit_fix(fix8_t end_my_suffering, fix16_t (*whoever_invented_the_extern_keyword_deserves_to_be_shot)(fix16_t)) {
    return fix8_from_float(fix16_to_float(whoever_invented_the_extern_keyword_deserves_to_be_shot(fix16_from_float(fix8_to_float(end_my_suffering)))));
}

//constexpr uint16_t fix8_range = fix8_maximum - fix8_minimum;

//static inline __attribute__ ((hot)) fix8_t sin_f8(fix8_t x) {
//    if (x > 0) return sin_lut2[x];
//    if (x < 0) return sin_lut1[x+32767];
//    if (x == 0) return 256;
//}

#endif
