#include <Arduino.h>

#ifndef interp1d_u16_h
#define interp1d_u16_h

template <uint16_t size>
class Interp1d_u16{
    // We assume key and value arrays are parallel
    // We assume key is constantly increasing
    uint16_t key[size];
    uint16_t value[size];
    uint16_t min;
    uint16_t max;

    public:
        Interp1d_u16(uint16_t key_in[size], uint16_t value_in[size]);
        uint16_t operator()(uint16_t key);
};


template <uint16_t size>
Interp1d_u16<size>::Interp1d_u16(uint16_t keyIn[size], uint16_t valueIn[size]) {
    memcpy(key, keyIn, size*sizeof(uint16_t));
    memcpy(value, valueIn, size*sizeof(uint16_t));
    min = key[0];
    max = key[size-1];
}

template <uint16_t size>
uint16_t Interp1d_u16<size>::operator()(uint16_t interp) {
    if (interp <= min) return min;
    if (interp >= max) return max;
    uint16_t incr = 0;
    // Find the first key greater than the interpolating
    while (key[incr] < interp && incr < size) incr++;
    uint16_t lowIdx = incr-1;
    uint16_t highIdx = incr;

    uint16_t keyLow = key[lowIdx];
    uint16_t keyHigh = key[highIdx];

    //std::cout << "Low key:" << keyLow << " High key: " << keyHigh << std::endl;

    uint16_t valLow = value[lowIdx];
    uint16_t valHigh = value[highIdx];

    int32_t deltaKey = keyHigh-keyLow;
    int32_t deltaVal = ((int32_t)valHigh)-((int32_t)valLow);
    float ahead = interp - keyLow;
    //std::cout << "Ahead: " << ahead << std::endl;
    //std::cout << "Low val:" << valLow << " High val: " << valHigh << std::endl;
    //std::cout << "dv:" << deltaKey << " dk: " << deltaVal << std::endl;

    uint16_t val = valLow + ((int16_t) (ahead*(((float) deltaVal)/((float)deltaKey))));
    return val;
}

#endif
