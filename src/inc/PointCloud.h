#ifndef pointcloud_h
#define pointcloud_h

#include "ScanUtils.h"

#include <Arduino.h>
#include "fix16.h"
#include "fix8.h"
#include "main.h"

//inline fix8_t deg2rad(fix8_t deg) { // deg * pi / 180 == deg*(pi/2)/(90)
//    return fix8_div(fix8_mul(deg, fix8_div(FIX_PI,fix8_from_int(2))), fix8_from_int(90));
//}

inline fix16_t deg2rad(fix16_t deg) { 
    //return fix16_mul(deg, fix16_from_float(0.017453292519943295));
    return fix16_div(fix16_mul(deg, fix16_from_float(M_PI)), fix16_from_int(180));
}

void polarToCartesian(Point *pt);

void pointCloudToCartesian(Point array[NUM_PAN_POINTS][NUM_TILT_POINTS]);

inline bool pointValid(Point *pt) {
    if (pt->r > 1000) return false;
    return true;
}

constexpr Point POINT_ORIGIN {int2fix8(0), int2fix8(0), int2fix8(0)};

#endif
