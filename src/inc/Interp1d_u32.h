#include <Arduino.h>

#ifndef interp1d_u32_h
#define interp1d_u32_h

template <uint16_t size>
class Interp1d_u32{
    // We assume key and value arrays are parallel
    // We assume key is constantly increasing
    uint32_t key[size];
    uint32_t value[size];
    uint32_t min;
    uint32_t max;

    public:
        Interp1d_u32(uint32_t key_in[size], uint32_t value_in[size]);
        uint32_t operator()(uint32_t key);
};


template <uint16_t size>
Interp1d_u32<size>::Interp1d_u32(uint32_t keyIn[size], uint32_t valueIn[size]) {
    memcpy(key, keyIn, size*sizeof(uint32_t));
    memcpy(value, valueIn, size*sizeof(uint32_t));
    min = key[0];
    max = key[size-1];
}

template <uint16_t size>
uint32_t Interp1d_u32<size>::operator()(uint32_t interp) {
    if (interp <= min) return min;
    if (interp >= max) return max;
    uint16_t incr = 0;
    // Find the first key greater than the interpolating
    while (key[incr] < interp && incr < size) incr++;
    uint16_t lowIdx = incr-1;
    uint16_t highIdx = incr;

    uint32_t keyLow = key[lowIdx];
    uint32_t keyHigh = key[highIdx];

    //std::cout << "Low key:" << keyLow << " High key: " << keyHigh << std::endl;

    uint32_t valLow = value[lowIdx];
    uint32_t valHigh = value[highIdx];

    int64_t deltaKey = keyHigh-keyLow;
    int64_t deltaVal = ((int64_t)valHigh)-((int64_t)valLow);
    float ahead = interp - keyLow;
    //std::cout << "Ahead: " << ahead << std::endl;
    //std::cout << "Low val:" << valLow << " High val: " << valHigh << std::endl;
    //std::cout << "dv:" << deltaKey << " dk: " << deltaVal << std::endl;

    uint32_t val = valLow + ((int32_t) ahead*(((float) deltaVal)/((float)deltaKey)));
    return val;
}

#endif
