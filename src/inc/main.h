#ifndef main_h
#define main_h

#include <Arduino.h>
#include "fix16.h"
#include "utils.h"
//#include <math.h>
#include "fix8.h"

#define __used __attribute__ ((used))
#define __pure __attribute__ ((pure))
#define __hot __attribute__ ((hot))
#define __alwaysinline __attribute__ ((always_inline))

constexpr fix16_t PAN_ANGLE_MIN = float2fix16(-30.0);
constexpr fix16_t PAN_ANGLE_MAX = float2fix16(30.0);
constexpr fix16_t TILT_ANGLE_MIN = float2fix16(-30.0);
constexpr fix16_t TILT_ANGLE_MAX = float2fix16(45.0);

//constexpr fix16_t PAN_ANGLE_MIN = float2fix16(-45.0);
//constexpr fix16_t PAN_ANGLE_MAX = float2fix16(45.0);
//constexpr fix16_t TILT_ANGLE_MIN = float2fix16(-45.0);
//constexpr fix16_t TILT_ANGLE_MAX = float2fix16(45.0);

constexpr fix16_t PAN_MIN_POSSIBLE = float2fix16(-45.0);
constexpr fix16_t PAN_MAX_POSSIBLE = float2fix16(45.0);
constexpr fix16_t TILT_MIN_POSSIBLE = float2fix16(-45.0);
constexpr fix16_t TILT_MAX_POSSIBLE = float2fix16(45.0);

static inline fix16_t fix8_to_fix16(fix8_t in) {
    return fix16_from_float(fix8_to_float(in));
}

static inline fix8_t fix16_to_fix8(fix16_t in) {
    return fix8_from_float(fix16_to_float(in));
}

const fix16_t PAN_MIN_POSSIBLE_F8 = fix16_to_fix8(PAN_MIN_POSSIBLE);
const fix16_t PAN_MAX_POSSIBLE_F8 = fix16_to_fix8(PAN_MAX_POSSIBLE);
const fix16_t TILT_MIN_POSSIBLE_F8 = fix16_to_fix8(TILT_MIN_POSSIBLE);
const fix16_t TILT_MAX_POSSIBLE_F8 = fix16_to_fix8(TILT_MAX_POSSIBLE);

//#define PAN_ANGLE_MIN  float2fix16(-45.0)
//#define PAN_ANGLE_MAX  float2fix16(45.0)
//#define TILT_ANGLE_MIN  float2fix16(0)
//#define TILT_ANGLE_MAX  float2fix16(45)

typedef struct LUTMapEntry {
    uint16_t k; 
    uint16_t v;
} LUTMapEntry;

#define NUM_PAN_POINTS 30
#define NUM_TILT_POINTS 38

#define DWELL_MS 200
#define ROLLOVER_ADD_DWELL_MS 200
//#define DWELL_MS 0

#define IR_PORT A0
#define PAN_SERVO_PORT 7
#define TILT_SERVO_PORT 8
#define VL53L1X_PWM_PIN 2
#define LED_PIN 13
#define BUZZER_PIN 3
#define SERVO_POWER_FET_PIN 21

#define NUM_DISTANCE_READINGS_TO_AVERAGE 3
#define TIME_BETWEEN_READINGS_MS 170

#define PAN_NEG45_US 1102
#define PAN_POS45_US 1929
#define TILT_NEG45_US 1152
#define TILT_POS45_US 1978

constexpr fix8_t FIX_PI = float2fix8(M_PI);
//constexpr fix8_t FIX_PI = fix8_pi;
// 205887 Maybe

void static inline __alwaysinline setLED(bool state) {
    digitalWrite(LED_PIN, !state);
}

void static inline __alwaysinline pulseLED(uint16_t millis) {
    setLED(true); delay(millis); setLED(false);
}

void static inline __alwaysinline setTone(uint16_t hz) {
    (hz > 0) ? tone(BUZZER_PIN, hz) : noTone(BUZZER_PIN);
}

void static inline __alwaysinline pulseTone(uint16_t hz, uint16_t millis) {
    setTone(hz); delay(millis); setTone(0);
}

void static inline __alwaysinline setServoPower(bool enable) {
    digitalWrite(SERVO_POWER_FET_PIN, enable ? HIGH : LOW);
} 

/*
 *
long map(long x, long in_min, long in_max, long out_min, long out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
*/

static fix8_t map_fix8_to_u32(fix8_t x, fix8_t in_min, fix8_t in_max, uint32_t out_min, uint32_t out_max) {
    fix16_t x16 = fix8_to_fix16(x);
    fix16_t in_min16 = fix8_to_fix16(in_min);
    fix16_t in_max16 = fix8_to_fix16(in_max);

    return fix16_to_int(fix16_sub(x16, in_min16)) * 
        (out_max - out_min) / fix16_to_int(fix16_sub(in_max16, in_min16))
        + out_min;
}


void setup();
void loop();

#endif
