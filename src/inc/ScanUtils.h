#ifndef ScanUtils_h
#define ScanUtils_h

#include "main.h"
#include <Arduino.h>
#include "fix16.h"
#include "fix8.h"
#include "ServoUtils.h"
#include "VL53L1XWrapper.h"

typedef struct Point {
    union {
        int16_t x;
        fix8_t theta;
    };
    union {
        int16_t y;
        fix8_t phi;
    };
    union {
        int16_t z;
        uint16_t r;
    };
} Point;

void fillScanAnglesList(
        Point array[NUM_PAN_POINTS][NUM_TILT_POINTS], 
        fix16_t minPanAngle, fix16_t maxPanAngle, uint8_t numPanPoints,
        fix16_t minTiltAngle, fix16_t maxTiltAngle, uint8_t numTiltPoints
        );

uint16_t getDistance(uint16_t (*distFcn)());

inline void move(fix8_t panAngle, fix8_t tiltAngle, Servo *panServo, Servo *tiltServo) {
    orient(
            panAngleToServoMicroseconds(panAngle),
            tiltAngleToServoMicroseconds(tiltAngle),
            panServo, tiltServo
          );
}
inline uint16_t moveAndScan(fix8_t panAngle, fix8_t tiltAngle, 
        uint16_t (*distFcn)(), Servo *panServo, Servo *tiltServo, uint16_t dwell) {
    move(panAngle, tiltAngle, panServo, tiltServo);
    delay(dwell);
    return getDistance(distFcn);
}

void scanAll(
        Point array[NUM_PAN_POINTS][NUM_TILT_POINTS],
        uint16_t (*distFcn)(),
        Servo *panServo,
        Servo *tiltServo
        );

#endif
