#include <Arduino.h>
#include <fix16.h>

#ifndef interp1d_1616_h
#define interp1d_1616_h

template <uint16_t size>
class Interp1d_f1616{
    // We assume key and value arrays are parallel
    // We assume key is constantly increasing
    fix16_t key[size];
    fix16_t value[size];
    fix16_t min;
    fix16_t max;

    public:
        Interp1d_f1616(fix16_t key_in[size], fix16_t value_in[size]);
        fix16_t operator()(fix16_t key);
};


template <uint16_t size>
Interp1d_f1616<size>::Interp1d_f1616(fix16_t keyIn[size], fix16_t valueIn[size]) {
    memcpy(key, keyIn, size*sizeof(fix16_t));
    memcpy(value, valueIn, size*sizeof(fix16_t));
    min = key[0];
    max = key[size-1];
}

template <uint16_t size>
fix16_t Interp1d_f1616<size>::operator()(fix16_t interp) {
    if (interp <= min) return min;
    if (interp >= max) return max;
    uint16_t incr = 0;
    // Find the first key greater than the interpolating
    while (key[incr] < interp && incr < size) incr++;
    uint16_t lowIdx = incr-1;
    uint16_t highIdx = incr;

    fix16_t keyLow = key[lowIdx];
    fix16_t keyHigh = key[highIdx];

    //std::cout << "Low key:" << keyLow << " High key: " << keyHigh << std::endl;

    fix16_t valLow = value[lowIdx];
    fix16_t valHigh = value[highIdx];

    fix16_t deltaKey = fix16_sub(keyHigh, keyLow);
    fix16_t deltaVal = fix16_sub(valHigh, valLow);
    fix16_t ahead = fix16_sub(interp, keyLow);
    //std::cout << "Ahead: " << ahead << std::endl;
    //std::cout << "Low val:" << valLow << " High val: " << valHigh << std::endl;
    //std::cout << "dv:" << deltaKey << " dk: " << deltaVal << std::endl;

    //uint32_t val = valLow + ((int32_t) ahead*(((float) deltaVal)/((float)deltaKey)));

    fix16_t div = fix16_div(deltaVal, deltaKey);
    fix16_t plusAhead = fix16_mul(ahead, div);
    fix16_t val = fix16_add(valLow, plusAhead);

    return val;
}

#endif
