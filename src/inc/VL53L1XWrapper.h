#ifndef VL53L1X_h
#define VL53L1X_h

#include <Arduino.h>
#include "Interp1d_u16.h"
#include "fix8.h"
#include "utils.h"
#include <avr/pgmspace.h> 
#include "SparkFun_VL53L1X.h"
#include "main.h"

#define VL_LUT_SIZE 20
constexpr LUTMapEntry VL53L1X_LUT[VL_LUT_SIZE] {
        {0, 0},
        {4096, 4000}
};

class VL53L1XWrapper {
    private:
        Interp1d_u16<VL_LUT_SIZE> *m;
    public:
        VL53L1XWrapper();
        //uint16_t operator () (uint16_t adc);
        uint16_t operator () ();
        void checkLUT();
};

#endif
