#ifndef printutil_h
#define printutil_h

#include "main.h"
#include "fix8.h"
#include "ScanUtils.h"

#define PRINT_DECIMALS 3

constexpr char jsonStartText[] = "{\"pointcloud\": [";
constexpr char jsonEndText[] = "]}";
constexpr char listEntryText[] = "[%d,%d,%d],";
constexpr char listEntryTextEnd[] = "[%d,%d,%d]";

static uint8_t getNeededBufSize() {
    char buf[50] = "";
    uint8_t curr = 0;
    fix16_to_str(fix16_from_float(fix8_to_float(fix8_minimum)), buf, PRINT_DECIMALS);
    if (strlen(buf) > curr) curr = strlen(buf);
    fix16_to_str(fix16_from_float(fix8_to_float(fix8_maximum)), buf, PRINT_DECIMALS);
    if (strlen(buf) > curr) curr = strlen(buf);
    fix16_to_str(fix16_from_float(fix8_to_float(FIX_PI)), buf, PRINT_DECIMALS);
    if (strlen(buf) > curr) curr = strlen(buf);
    return curr;
}

//const uint8_t fix1616bufsize = getNeededBufSize();
constexpr uint8_t fix8bufsize = 10;

void printPointCloud(Point array[NUM_PAN_POINTS][NUM_TILT_POINTS]);

inline void fix82str(char buf[], fix8_t fix) {
    fix16_to_str(fix16_from_float(fix8_to_float(fix)), buf, PRINT_DECIMALS);
}

#endif
