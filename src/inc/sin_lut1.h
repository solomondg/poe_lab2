#ifndef sin_lut1_h
#define sin_lut1_h

#include "fix8.h"
#include <avr/pgmspace.h>

#define LUT1_MIN -32768
#define LUT1_MAX -16385

#define LUT2_MIN -16384
#define LUT2_MAX -1

#define LUT3_MIN 0
#define LUT3_MAX 16383

#define LUT4_MIN 16384
#define LUT4_MAX 32767

//const PROGMEM __attribute__ ((used)) fix8_t sin_lut1[LUT1_MAX-LUT1_MIN] = {};
//constexpr __attribute__ ((used, packed, progmem)) PROGMEM fix8_t sin_lut1[LUT1_MAX-LUT1_MIN] = {};
//constexpr __attribute__ ((used, packed, progmem)) PROGMEM fix8_t sin_lut2[LUT2_MAX-LUT2_MIN] = {};
//constexpr __attribute__ ((used, packed, progmem)) PROGMEM fix8_t sin_lut3[LUT3_MAX-LUT3_MIN] = {};
//constexpr __attribute__ ((used, packed, progmem)) PROGMEM fix8_t sin_lut4[LUT4_MAX-LUT4_MIN] = {};

#endif
