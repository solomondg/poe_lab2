#ifndef IRSensor_h
#define IRSensor_h

#include <Arduino.h>
#include "Interp1d_u16.h"
#include <fix16.h>
#include "utils.h"
#include <avr/pgmspace.h> 
#include "main.h"


#define SENSOR_LUT_SIZE 20
constexpr LUTMapEntry SENSOR_LUT[SENSOR_LUT_SIZE] {
        {0, 0},
};

class IRSensor {
    private:
        Interp1d_u16<SENSOR_LUT_SIZE> *m;
        uint16_t port;
    public:
        IRSensor(uint16_t p);
        uint16_t operator () (uint16_t adc);
        uint16_t operator () ();
};

#endif
