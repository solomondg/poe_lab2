#ifndef ServoUtils_h
#define ServoUtils_h

#include "fix16.h"
#include "main.h"
#include "fix8.h"
#include "Servo.h"
#include <Arduino.h>



inline uint16_t panAngleToServoMicroseconds(fix8_t angle) {
    // lower (0°): 1102
    // upper (90°): 1929
    return map_fix8_to_u32(angle, 
            PAN_MIN_POSSIBLE_F8, PAN_MAX_POSSIBLE_F8, 
            PAN_NEG45_US, PAN_POS45_US
            );
}

inline uint16_t tiltAngleToServoMicroseconds(fix8_t angle) {
    // bottom: 1152
    // center: 1565
    // top: 1978
    return map_fix8_to_u32(angle, 
            TILT_MIN_POSSIBLE_F8, TILT_MAX_POSSIBLE_F8, 
            TILT_NEG45_US, TILT_POS45_US
            );
}

void fillScanArray(uint16_t fillArray[], int16_t minAngle, int16_t maxAngle, uint16_t numPoints, uint16_t (*convfcn)(fix16_t));

inline void orient(uint16_t pan_us, uint16_t tilt_us, Servo *panServo, Servo *tiltServo) {
    panServo->writeMicroseconds(pan_us);
    tiltServo->writeMicroseconds(tilt_us);
}

#endif
